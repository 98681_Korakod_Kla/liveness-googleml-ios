# Liveness-GoogleML-IOS



## What is it

This is POC liveness check for face recognition using [Google ML kit](https://developers.google.com/ml-kit/guides)

## Setup

1. Clone project

2.  `pod install`

3. download ML model (using for object detection not face) ->
 [ML model ](https://tfhub.dev/tensorflow/lite-model/efficientnet/lite4/int8/2)

4. copy ML model to xcode project

## result

Liveness check is working perfectly but still not have anything to prevent sproofing even we using gesture or activity to proof human.

Pro - free, for face detection is quite lightweight also can integrate object detection to find mask or hat, easy to custom UI and flow logic like when to capture what size to capture.

Con - if we using object detection might increase app size and require additional download model. Need pod for install, might need gitlarge file to store version control


## Screen shot
![image info](view.PNG){width=20%}
![image info](scan.PNG){width=20%}
![image info](story.png){width=50%}

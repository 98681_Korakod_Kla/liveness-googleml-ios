//
//  ViewController.swift
//  LeoScan
//
//  Created by Korakod Saraboon on 26/10/2566 BE.
//

import UIKit

class ViewController: UIViewController {

  @IBOutlet weak var logoImage: UIImageView!
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setup()
  }

  private func setup() {
    self.navigationController?.isNavigationBarHidden = true
    let image = UIImage(named: "face-scan")?.withRenderingMode(.alwaysTemplate)
    logoImage.image = image
    logoImage.tintColor = .white
  }
  
  @IBAction func startAction(_ sender: Any) {
    // navigate to new view
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    let vc = storyboard.instantiateViewController(withIdentifier: "facescan")
    navigationController?.pushViewController(vc, animated: true)
  }
  
}


//
//  FaceScanViewController.swift
//  LeoScan
//
//  Created by Korakod Saraboon on 26/10/2566 BE.
//

import UIKit
import MLKit

class FaceScanViewController: UIViewController {
  @IBOutlet weak var cameraView: CameraView!
  @IBOutlet weak var containerView: UIView!
  
  // response element
  @IBOutlet weak var blinkCheckView: UIView!
  @IBOutlet weak var smileCheckView: UIView!
  @IBOutlet weak var turnLeftCheckView: UIView!
  @IBOutlet weak var turnRightCheckView: UIView!
  
  @IBOutlet weak var hatCheckView: UIButton!
  @IBOutlet weak var maskCheckView: UIButton!
  @IBOutlet weak var glassCheckView: UIButton!
  
  @IBOutlet weak var checkLabel: UILabel!
  
  var faceOptions = FaceDetectorOptions()
  var objectOptions: CustomObjectDetectorOptions?
  
  var isSmile = false
  var isBlink = false
  var isLeft = false
  var isRight = false
  
  var isHat = false
  var isMask = false
  var isGlass = false
  
  let livenessCheck = "Liveness Checking..."
  let livenessPass = "Liveness Pass"
  
  let objectMLpath =  Bundle.main.path(forResource: "efficientnet_lite4_int8_2", ofType: "tflite")
  
  override func viewDidLoad() {
    setupView()
    setupML()
  }
  
  private func setupView(){
    containerView.layer.cornerRadius = containerView.bounds.width/2
    containerView.layer.borderWidth = 10
    containerView.layer.borderColor = UIColor.white.cgColor
    containerView.clipsToBounds = true
    cameraView.delegate = self
  }
  
  private func setupML() {
    faceOptions.performanceMode = .accurate
    faceOptions.landmarkMode = .all
    faceOptions.classificationMode = .all
    
    print(objectMLpath)
    
    let localModel = LocalModel(path: objectMLpath!)
    objectOptions = CustomObjectDetectorOptions(localModel: localModel)
    objectOptions?.shouldEnableClassification = true
    objectOptions?.maxPerObjectLabelCount = 3

  }
  
  private func resetLiveNess() {
    isSmile = false
    isBlink = false
    isLeft = false
    isRight = false
    
    isHat = false
    isMask = false
    isGlass = false
    
    blinkCheckView.backgroundColor = .white
    smileCheckView.backgroundColor = .white
    turnLeftCheckView.backgroundColor = .white
    turnRightCheckView.backgroundColor = .white
    
    checkLabel.text = livenessCheck
    checkLabel.textColor = .white
  }
  
  func checkLiveness() {
    // update ui base on liveness and object check
    blinkCheckView.backgroundColor = isBlink ? .green : .white
    smileCheckView.backgroundColor = isSmile ? .green : .white
    turnLeftCheckView.backgroundColor = isLeft ? .green : .white
    turnRightCheckView.backgroundColor = isRight ? .green : .white
    
    maskCheckView.isHidden = isMask ? false : true
    glassCheckView.isHidden = isGlass ? false : true
    hatCheckView.isHidden = isHat ? false : true
    
    let isObjectPass = (isHat || isMask || isGlass) ? false : true
    
    checkLabel.text = (isBlink && isSmile && isLeft && isRight && isObjectPass) ? livenessPass : livenessCheck
    checkLabel.textColor = (isBlink && isSmile && isLeft && isRight && isObjectPass) ? .green : .white
    
  
    // check pass and sent data to backend after this.
    //...
  }
  
  func livenessCheck(buffer: CMSampleBuffer) {
    let image = VisionImage(buffer: buffer)
    image.orientation = UIImage.Orientation.leftMirrored
    
    // face logic
    let faceDetector = FaceDetector.faceDetector(options: faceOptions)
    
    weak var weakSelf = self
    faceDetector.process(image) { faces, error in
      guard let strongSelf = weakSelf else {
        print("Self is nil!")
        return
      }
      guard error == nil, let faces = faces, !faces.isEmpty, faces.count == 1 else {
        self.containerView.layer.borderColor = UIColor.white.cgColor
        self.resetLiveNess()
        return
      }

      // Faces detected
      self.containerView.layer.borderColor = UIColor.green.cgColor
      
      for face in faces {
        
        // blink check
        if face.leftEyeOpenProbability < 0.4 || face.rightEyeOpenProbability < 0.4 {
          self.isBlink = true
        }
        
        // smile check
        if face.smilingProbability > 0.3 {
          self.isSmile = self.isMask ? false : true
        }
        
        // turn right
        if face.headEulerAngleY > 35 {
          self.isRight = true
        }

        // turn left
        if face.headEulerAngleY < -35 {
          self.isLeft = true
        }
      }
      
      // check liveness
      self.checkLiveness()
    }
  }
  
  func objectCheck(buffer: CMSampleBuffer) {
    let image = VisionImage(buffer: buffer)
    image.orientation = UIImage.Orientation.leftMirrored
    
    // object detect logic
    let objectDetector = ObjectDetector.objectDetector(options: objectOptions!)
    
    objectDetector.process(image) { objects, error in
        guard error == nil, let objects = objects, !objects.isEmpty else {
            // Handle the error.
            return
        }
        // Show results.
      for object in objects {
        /*  sample from google ml
        let frame = object.frame
        let trackingID = object.trackingID
        let description = object.labels.enumerated().map { (index, label) in
          "Label \(index): \(label.text), \(label.confidence), \(label.index)"
        }.joined(separator: "\n")
         */

        // mask check
        if (object.labels[0].text.contains("mask") ||
            object.labels[1].text.contains("mask") ||
            object.labels[2].text.contains("mask")) && object.labels[0].confidence > 0.1 {
          self.isMask = true
        } else {
          self.isMask = false
        }
        
        // glass
        if (object.labels[0].text.contains("glass") ||
            object.labels[1].text.contains("glass") ||
            object.labels[2].text.contains("glass"))  && object.labels[0].confidence > 0.1 {
          self.isGlass = true
        } else {
          self.isGlass = false
        }
        
        // hat
        if (object.labels[0].text.contains("hat") ||
            object.labels[1].text.contains("hat") ||
            object.labels[2].text.contains("hat"))  && object.labels[0].confidence > 0.1 {
          self.isHat = true
        } else {
          self.isHat = false
        }
      }
    }
  }
  
  
  @IBAction func backAction(_ sender: Any) {
    navigationController?.popViewController(animated: true)
  }
}

extension FaceScanViewController: CameraPreviewDelegate {
  func videoFeedCapture(buffer: CMSampleBuffer) {
    // send feed to liveness
    livenessCheck(buffer: buffer)
    // send feed to objectdetection
    objectCheck(buffer: buffer)
  }
}
